import Moleculer from 'moleculer';

const broker = new Moleculer.ServiceBroker();

broker.createService({
    name: 'string',
    actions: {
        capitalize(ctx) {
            const { word } = ctx.params;
            return `${word.charAt(0).toUpperCase()}${word.slice(1)}`;
        }
    }
});

broker.createService({
    name: 'person',
    actions: {
        async getFullName(ctx) {
            let firstName = 'john';
            let lastName = 'doe';
            
            firstName = await ctx.call('string.capitalize', { word: firstName});
            lastName = await ctx.call('string.capitalize', { word: lastName});

            const fullName = `${firstName} ${lastName}`;

            ctx.emit('greet', { fullName });

            return fullName;
        }
    }
});

broker.createService({
    name: 'greeter',
    events: {
        'greet': (ctx) => {
            const { fullName } = ctx.params;

            console.log(`[EVENT]: Hello ${fullName ? fullName : 'stranger'}`);
        }
    }
});

broker.start().then(() => broker.repl());