import Moleculer from 'moleculer';

const broker = new Moleculer.ServiceBroker();

broker.createService({
    name: 'math',
    actions: {
        add(ctx) {
            return Number(ctx.params.a) + Number(ctx.params.b);
        }
    }
});

broker.start()
    .then(() => broker.call('math.add', { a: 5, b: 3 }))
    .then(res => console.log('5 + 3 =', res))